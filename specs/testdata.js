module.exports = [
  {
    format: 'markdown',
    slug: '123',
    title: '123'
  },
  {
    format: 'markdown',
    slug: 'page-#10',
    title: 'Page #10'
  },
  {
    format: 'markdown',
    slug: 'test/a',
    title: 'A'
  },
  {
    format: 'markdown',
    slug: 'test/b',
    title: 'B'
  },
  {
    format: 'markdown',
    slug: 'test/foo/bar',
    title: 'Bar'
  },
  {
    format: 'markdown',
    slug: 'test/nothing/here/but-here',
    title: 'But here'
  }
];
