const {
  slugSplit,
  createFile,
  createFolders,
  getFolderIndex
} = require('../src/utils/index');

describe('slugSplit', () => {
  it('returns an object with two functions', () => {
    const slugObj = slugSplit();
    expect(slugObj).toEqual(
      expect.objectContaining({
        folders: expect.any(Function),
        file: expect.any(Function)
      })
    );
    expect(slugObj.folders()).toEqual([]);
    expect(slugObj.file()).toEqual('');
  });

  it('correctly splits a string on the / character', () => {
    const slugObj = slugSplit('foo/bar/baz');
    expect(slugObj.folders()).toEqual(['foo', 'bar']);
    expect(slugObj.file()).toEqual('baz');
  });

  it('returns a fresh copy of the folders everytime its called', () => {
    const slugObj = slugSplit('foo/bar/baz');
    const batch1 = slugObj.folders();
    const batch2 = slugObj.folders();
    batch1.splice(0, 2);
    expect(batch1.length).toBe(0);
    expect(batch2.length).toBe(2);
  });
});

test('getFolderIndex', () => {
  const fakeTree = [
    {},
    { title: 'not', type: 'folder' },
    { title: 'here', type: 'folder' }
  ];

  const index = getFolderIndex('here', fakeTree);
  expect(index).toBe(2);
});

describe('createFolders', () => {
  const slug = 'this/should-be/a/tree';
  const slugObj = slugSplit(slug);

  it('creates folder structure based on a slug', () => {
    const target = [];
    createFolders(slugObj.folders(), target);

    expect(target).toMatchInlineSnapshot(`
      Array [
        Object {
          "children": Array [
            Object {
              "children": Array [
                Object {
                  "children": Array [],
                  "title": "a",
                  "type": "folder",
                },
              ],
              "title": "should-be",
              "type": "folder",
            },
          ],
          "title": "this",
          "type": "folder",
        },
      ]
    `);
  });
});

describe('createFile', () => {
  const testFile = {
    format: 'markdown',
    slug: 'test/nothing/here/but-here',
    title: 'But here'
  };
  const slugObj = slugSplit(testFile.slug);

  it('should add a file to the root if no folders provided', () => {
    const target = [];
    createFile([], testFile, target);
    expect(target[0]).toEqual(
      expect.objectContaining({
        type: 'file',
        ...testFile
      })
    );
  });

  it('should add file in existing folder structure', () => {
    const target = [];
    createFolders(slugObj.folders(), target);
    createFile(slugObj.folders(), testFile, target);
    expect(target).toMatchInlineSnapshot(`
      Array [
        Object {
          "children": Array [
            Object {
              "children": Array [
                Object {
                  "children": Array [
                    Object {
                      "format": "markdown",
                      "slug": "test/nothing/here/but-here",
                      "title": "But here",
                      "type": "file",
                    },
                  ],
                  "title": "here",
                  "type": "folder",
                },
              ],
              "title": "nothing",
              "type": "folder",
            },
          ],
          "title": "test",
          "type": "folder",
        },
      ]
    `);
  });
});
