const Treeify = require('../src/treeify');
const testData = require('./testdata');

test('transducer works', () => {
  const treeify = new Treeify();

  const struct = treeify.that(testData);
  expect(struct).toMatchSnapshot();
});
