# Treeify

![image](https://gitlab.com/codebryo/treeify/badges/master/pipeline.svg)

Create a tree structure based on relative slugs.

## What's it for

Inspired by the needs of turning a flat hirarchy structure into a proper object tree that can be used to render components on a page.

Let's say you have an object like this:

```js
{
  title: 'super',
  slug: 'some/nested/folders/super'
}
```

And you want to turn this into a tree-like structure for rendering, so you want something like this:

```js
[
  {
    type: 'folder',
    title: 'some',
    children: [
      {
        type: 'folder',
        title: 'nested',
        children: [
          {
            type: 'folder',
            title: 'folders',
            children: [
              {
                type: 'file',
                title: 'super',
                slug: 'some/nested/folders/super'
              }
            ]
          }
        ]
      }
    ]
  }
];
```

Then this is for you.

## How does it work

This is currently written in node only using commonjs modules. To get started you need to load the `treeify.js` file.

```js
const Treeify = require('./src/treeify')

const treeify = new Treeify() // Will be customizeable

const treeStructure = treeify.that([...])
```

If you are curious, about some more examples and how it's all working, check out the [specs folder.](./specs/)

## Upcoming

- [ ] Add support for configuration values
- [ ] Automatically freeze the output for better performance
- [ ] Customize the main key (currently title)
- [ ] Better release process (npm, esm build)
- [ ] Typescript support
