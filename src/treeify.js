const { slugSplit, createFile, createFolders } = require('./utils/index');

module.exports = class Treeify {
  constructor() {}

  that(arr) {
    const transducer = (acc, entry) => {
      const slugObj = slugSplit(entry.slug);
      createFolders(slugObj.folders(), acc);
      createFile(slugObj.folders(), entry, acc);
      return acc;
    };

    return arr.reduce(transducer, []);
  }
};
