/**
 * Split a string into folders and a file
 *
 * @param {String} slug
 */
function slugSplit(slug = '') {
  const fragments = slug.split('/');

  const size = fragments.length;
  const file = fragments[size - 1];
  const folders = size > 1 ? fragments.slice(0, size - 1) : [];

  return {
    folders: () => [...folders],
    file: () => file
  };
}

/**
 *
 * @param {String} folderName
 */
function createNewFolderObject(folderName) {
  return { type: 'folder', title: folderName, children: [] };
}

/**
 *
 * @param {Object} originalFile
 */
function createNewFileObject(originalFile) {
  return { type: 'file', ...originalFile };
}

/**
 * Find the index of a folder object, based on the title
 *
 * @param {String} folderName
 * @param {Array} arr
 */
function getFolderIndex(folderName, arr) {
  return arr.findIndex(entry => {
    return entry.type === 'folder' && entry.title === folderName;
  });
}

/**
 * Recursive function to create neste folder structure
 *
 * @param {Array} foldersArray
 * @param {Array} targetRef
 */
function createFolders(foldersArray, targetRef) {
  if (foldersArray.length === 0) return;

  const folder = foldersArray.shift();
  const index = getFolderIndex(folder, targetRef);
  if (index >= 0) return createFolders(foldersArray, targetRef[index].children);

  targetRef.push(createNewFolderObject(folder));
  return createFolders(foldersArray, targetRef[targetRef.length - 1].children);
}

/**
 *
 * @param {Array} foldersArray
 * @param {Object} fileObject
 * @param {Array} target
 */
function createFile(foldersArray, fileObject, target) {
  let currentRef = target;

  // Traverse the tree to find the latest folder to create the file in
  foldersArray.forEach(folder => {
    currentRef = currentRef[getFolderIndex(folder, currentRef)].children;
  });

  return currentRef.push(createNewFileObject(fileObject));
}

module.exports = {
  slugSplit,
  createFolders,
  createFile,
  getFolderIndex
};
